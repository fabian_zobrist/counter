package me.zobrist.tichucounter

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class HistoryUnitTest {
    @Test
    fun calculation_isCorrect() {
        val history = History()

        history.revertLastRound()
        history.getHistoryA()
        history.getHistoryB()
        history.getScoreA()
        history.getScoreB()

        history.logRound(Round(10, 10))
        history.logRound(Round(10, 10))
        history.logRound(Round(10, 10))
        history.logRound(Round(10, 10))
        history.logRound(Round(10, 10))
        history.logRound(Round(10, 10))
        history.logRound(Round(10, 10))
        history.logRound(Round(10, 10))
        history.logRound(Round(10, 10))
        history.logRound(Round(10, 10))

        assertEquals(100, history.getScoreA())
        assertEquals(100, history.getScoreB())

        history.revertLastRound()

        assertEquals(90, history.getScoreA())
        assertEquals(90, history.getScoreB())

        assertNotEquals("", history.getHistoryA())
        assertNotEquals("", history.getHistoryB())

        history.clearAll()
        assertEquals(0, history.getScoreA())
        assertEquals(0, history.getScoreB())


        assertEquals("", history.getHistoryA())
        assertEquals("", history.getHistoryB())
    }
}