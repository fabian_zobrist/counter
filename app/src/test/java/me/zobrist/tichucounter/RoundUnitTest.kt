package me.zobrist.tichucounter

import org.junit.Assert.*
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class RoundUnitTest {
    @Test
    fun calculation_isCorrect() {

        var inputScoreA = 125
        var inputScoreB = -25
        var temp: Round

        // Normal round range -25 to 125 as input
        while (inputScoreB >= 125) {
            temp = Round(inputScoreA, true)
            assertEquals(inputScoreB, temp.scoreB)
            assertTrue(temp.isValidRound())

            temp = Round(inputScoreA, false)
            assertEquals(inputScoreB, temp.scoreA)
            assertTrue(temp.isValidRound())

            inputScoreA -= 5
            inputScoreB += 5
        }

        // Team a +100 points for Tichu
        inputScoreA = 125 + 100
        inputScoreB = -25

        // Normal round range -25 to 125 as input
        while (inputScoreB >= 125) {
            temp = Round(inputScoreA, true)
            assertEquals(inputScoreB, temp.scoreB)
            assertTrue(temp.isValidRound())

            temp = Round(inputScoreA, false)
            assertEquals(inputScoreB, temp.scoreA)
            assertTrue(temp.isValidRound())

            inputScoreA -= 5
            inputScoreB += 5
        }

        // Double win
        temp = Round(200, true)
        assertEquals(0, temp.scoreB)
        assertTrue(temp.isValidRound())

        temp = Round(200, false)
        assertEquals(0, temp.scoreA)
        assertTrue(temp.isValidRound())

        // Double win with Tichu
        temp = Round(300, true)
        assertEquals(0, temp.scoreB)
        assertTrue(temp.isValidRound())

        temp = Round(300, false)
        assertEquals(0, temp.scoreA)
        assertTrue(temp.isValidRound())

        // Double win with Grand Tichu
        temp = Round(400, true)
        assertEquals(0, temp.scoreB)
        assertTrue(temp.isValidRound())

        temp = Round(400, false)
        assertEquals(0, temp.scoreA)
        assertTrue(temp.isValidRound())

        //Good rounds
        temp = Round(0, 0)
        assertTrue(temp.isValidRound())

        //Bad rounds
        temp = Round(5, 12)
        assertFalse(temp.isValidRound())

        temp = Round(12, 5)
        assertFalse(temp.isValidRound())

        temp = Round(5, 55)
        assertFalse(temp.isValidRound())
    }
}