package me.zobrist.tichucounter

import java.io.Serializable

class Round() : Serializable {
    var scoreA: Int = 0
    var scoreB: Int = 0

    constructor(score: Int, isScoreA: Boolean) : this() {
        if (isScoreA) {
            scoreA = score
            scoreB = calculateOtherScore(scoreA)
        } else {
            scoreB = score
            scoreA = calculateOtherScore(scoreB)
        }
    }

    constructor(scoreA: Int, scoreB: Int) : this() {
        this.scoreA = scoreA
        this.scoreB = scoreB
    }

    private fun calculateOtherScore(score: Int): Int {
        if (isMultipleOf100(score)) {
            return 0
        }
        return 100 - (score % 100)
    }

    private fun isMultipleOf100(score: Int): Boolean {
        return (score / 100) >= 1 && (score % 100) == 0
    }

    fun isValidRound(): Boolean {
        return (scoreA % 5 == 0) && (scoreB % 5 == 0) && ((scoreA + scoreB) % 100 == 0)
    }
}