@file:Suppress("unused")

package me.zobrist.tichucounter


class History {
    private var scores: ArrayList<Round> = ArrayList()

    fun getScoreA(): Int {
        var tempScore = 0
        scores.forEach {
            tempScore += it.scoreA
        }
        return tempScore
    }

    fun getScoreB(): Int {
        var tempScore = 0
        scores.forEach {
            tempScore += it.scoreB
        }
        return tempScore
    }

    fun getHistoryA(): String {
        var tempHistory = String()
        scores.forEach {
            tempHistory += it.scoreA.toString() + "\n"
        }
        return tempHistory
    }

    fun getHistoryB(): String {
        var tempHistory = String()
        scores.forEach {
            tempHistory += it.scoreB.toString() + "\n"
        }
        return tempHistory
    }

    fun logRound(round: Round) {
        scores.add(round)
    }

    fun revertLastRound() {
        if (scores.isNotEmpty()) {
            scores.removeAt(scores.size - 1)
        }
    }

    fun clearAll() {
        scores.clear()
    }

    fun isEmpty(): Boolean {
        return scores.isEmpty()
    }
}