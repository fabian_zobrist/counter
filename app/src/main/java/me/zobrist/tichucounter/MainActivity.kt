package me.zobrist.tichucounter

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ScrollView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.widget.doOnTextChanged
import com.google.gson.Gson
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    private var updateOnChange: Boolean = true

    private lateinit var history: History
    private var currentRound = Round()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        inputTeamA.setRawInputType(InputType.TYPE_NULL)
        inputTeamB.setRawInputType(InputType.TYPE_NULL)
        inputTeamA.requestFocus()
        disableSubmitButton()
        updateTheme(this.getSharedPreferences("Settings", Context.MODE_PRIVATE).getInt("Theme", 2))
        keepScreenOn(
            this.getSharedPreferences("Settings", Context.MODE_PRIVATE)
                .getBoolean("Screen_On", false)
        )

        val json = this.getSharedPreferences("Settings", Context.MODE_PRIVATE).getString("history", "{\"scores\":[]}")
        history = Gson().fromJson(json, History::class.java)
        nameTeamA.setText(this.getSharedPreferences("Settings", Context.MODE_PRIVATE).getString("nameTeamA", "TeamA"))
        nameTeamB.setText(this.getSharedPreferences("Settings", Context.MODE_PRIVATE).getString("nameTeamB", "TeamB"))
        updateView()


        inputTeamA.setOnFocusChangeListener { view, b ->
            if (b) {
                hideKeyboard()
            }
        }

        inputTeamB.setOnFocusChangeListener { view, b ->
            if (b) {
                hideKeyboard()
            }
        }

        inputTeamA.doOnTextChanged { text, start, count, after ->
            if (inputTeamA.isFocused) {
                if (inputTeamA.text.isNotEmpty()) {
                    if (updateOnChange) {
                        currentRound = try {
                            Round(text.toString().toInt(), true)

                        } catch (e: java.lang.Exception) {
                            Round(1, 1)
                        }
                        inputTeamB.setText(currentRound.scoreB.toString())
                    } else {
                        updateOnChange = true
                    }
                } else {
                    inputTeamA.text.clear()
                    inputTeamB.text.clear()
                }
            }

            if (currentRound.isValidRound() && inputTeamA.text.isNotEmpty() && inputTeamB.text.isNotEmpty()) {
                enableSubmitButton()
            } else {
                disableSubmitButton()
            }
        }



        inputTeamB.doOnTextChanged { text, start, count, after ->
            if (inputTeamB.isFocused) {
                if (inputTeamB.text.isNotEmpty()) {
                    if (updateOnChange) {
                        currentRound = try {
                            Round(text.toString().toInt(), false)

                        } catch (e: java.lang.Exception) {
                            Round(1, 1)
                        }
                        inputTeamA.setText(currentRound.scoreA.toString())

                    } else {
                        updateOnChange = true
                    }

                } else {
                    inputTeamA.text.clear()
                    inputTeamB.text.clear()
                }
            }

            if (currentRound.isValidRound() && inputTeamA.text.isNotEmpty() && inputTeamB.text.isNotEmpty()) {
                enableSubmitButton()
            } else {
                disableSubmitButton()
            }
        }

        buttonAdd100.setOnClickListener {
            giveFocusToAIfNone()

            if (inputTeamA.isFocused) {

                currentRound.scoreA = try {
                    inputTeamA.text.toString().toInt() + 100
                } catch (e: Exception) {
                    currentRound.scoreB = 0
                    inputTeamB.setText(currentRound.scoreB.toString())
                    100
                }
                updateOnChange = false
                inputTeamA.setText(currentRound.scoreA.toString())
            }

            if (inputTeamB.isFocused) {
                currentRound.scoreB = try {
                    inputTeamB.text.toString().toInt() + 100
                } catch (e: Exception) {
                    currentRound.scoreA = 0
                    inputTeamA.setText(currentRound.scoreA.toString())
                    100

                }
                updateOnChange = false
                inputTeamB.setText(currentRound.scoreB.toString())

            }
        }

        buttonSub100.setOnClickListener {
            giveFocusToAIfNone()

            if (inputTeamA.isFocused) {
                currentRound.scoreA = try {
                    inputTeamA.text.toString().toInt() - 100
                } catch (e: Exception) {
                    currentRound.scoreB = 0
                    inputTeamB.setText(currentRound.scoreB.toString())
                    -100
                }
                updateOnChange = false
                inputTeamA.setText(currentRound.scoreA.toString())
            }

            if (inputTeamB.isFocused) {
                currentRound.scoreB = try {
                    inputTeamB.text.toString().toInt() - 100
                } catch (e: Exception) {
                    currentRound.scoreA = 0
                    inputTeamA.setText(currentRound.scoreA.toString())
                    -100
                }
                updateOnChange = false
                inputTeamB.setText(currentRound.scoreB.toString())
            }
        }

        button0.setOnClickListener {
            giveFocusToAIfNone()
            appendToFocusedInput('0')
        }

        button1.setOnClickListener {
            giveFocusToAIfNone()
            appendToFocusedInput('1')
        }

        button2.setOnClickListener {
            giveFocusToAIfNone()
            appendToFocusedInput('2')
        }

        button3.setOnClickListener {
            giveFocusToAIfNone()
            appendToFocusedInput('3')
        }

        button4.setOnClickListener {
            giveFocusToAIfNone()
            appendToFocusedInput('4')
        }

        button5.setOnClickListener {
            giveFocusToAIfNone()
            appendToFocusedInput('5')
        }

        button6.setOnClickListener {
            giveFocusToAIfNone()
            appendToFocusedInput('6')
        }

        button7.setOnClickListener {
            giveFocusToAIfNone()
            appendToFocusedInput('7')
        }

        button8.setOnClickListener {
            giveFocusToAIfNone()
            appendToFocusedInput('8')
        }

        button9.setOnClickListener {
            giveFocusToAIfNone()
            appendToFocusedInput('9')
        }

        buttonInv.setOnClickListener {
            val tempInt: Int

            giveFocusToAIfNone()

            if (inputTeamA.isFocused) {
                if (inputTeamA.text.toString().equals("-")) {
                    inputTeamA.text.clear()
                } else if (inputTeamA.text.isNotEmpty()) {
                    tempInt = inputTeamA.text.toString().toInt() * -1
                    inputTeamA.setText(tempInt.toString())
                } else {
                    updateOnChange = false
                    appendToFocusedInput('-')
                    currentRound = Round(1,1)
                }


            } else if (inputTeamB.isFocused) {
                if (inputTeamB.text.toString().equals("-")) {
                    inputTeamB.text.clear()
                } else if (inputTeamB.text.isNotEmpty()) {
                    tempInt = inputTeamB.text.toString().toInt() * -1
                    inputTeamB.setText(tempInt.toString())
                } else {
                    updateOnChange = false
                    appendToFocusedInput('-')
                    currentRound = Round(1,1)
                }
            }
        }

        buttonBack.setOnClickListener {
            giveFocusToAIfNone()

            if (inputTeamA.isFocused) {
                if (inputTeamA.text.isNotEmpty()) {
                    val string = inputTeamA.text.toString()
                    inputTeamA.setText(string.substring(0, string.length - 1))
                }

            } else if (inputTeamB.isFocused) {
                if (inputTeamB.text.isNotEmpty()) {
                    val string = inputTeamB.text.toString()
                    inputTeamB.setText(string.substring(0, string.length - 1))
                }
            }
        }

        submit.setOnClickListener {
            giveFocusToAIfNone()

            if (inputTeamA.text.isNotEmpty() && inputTeamB.text.isNotEmpty()) {

                history.logRound(
                    Round(
                        inputTeamA.text.toString().toInt(),
                        inputTeamB.text.toString().toInt()
                    )
                )

                updateView()

                inputTeamA.text.clear()
                inputTeamB.text.clear()
                disableSubmitButton()

                scrollViewHistory.fullScroll(ScrollView.FOCUS_DOWN)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val prefs = this.getSharedPreferences("Settings", Context.MODE_PRIVATE).edit()
        prefs.putString("history", Gson().toJson(history))
        prefs.putString("nameTeamA", nameTeamA.text.toString())
        prefs.putString("nameTeamB", nameTeamB.text.toString())
        prefs.apply()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        menu.findItem(R.id.action_screenOn).isChecked =
            this.getSharedPreferences("Settings", Context.MODE_PRIVATE)
                .getBoolean("Screen_On", false)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_clear -> {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(getString(R.string.confirmClear))
                    .setTitle(R.string.clear)
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.yes)) { dialog, id ->
                        dialog.dismiss()
                        clearAll()
                    }
                    .setNegativeButton(getString(R.string.no)) { dialog, id ->
                        dialog.cancel()
                    }

                builder.create().show()
                true
            }
            R.id.action_undo -> {
                undoLastRound()
                true
            }
            R.id.action_theme -> {
                chooseThemeDialog()
                true
            }
            R.id.action_screenOn -> {
                item.isChecked = !item.isChecked
                keepScreenOn(item.isChecked)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager =
            getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }

    private fun giveFocusToAIfNone() {
        if (!inputTeamA.isFocused && !inputTeamB.isFocused) {
            inputTeamA.requestFocus()
        }
    }

    private fun undoLastRound() {
        history.revertLastRound()
        updateView()
    }

    private fun updateView() {
        scoreA.text = history.getScoreA().toString()
        scoreB.text = history.getScoreB().toString()

        historyA.text = history.getHistoryA()
        historyB.text = history.getHistoryB()
    }

    private fun clearAll() {
        historyA.text = ""
        historyB.text = ""
        inputTeamA.text.clear()
        inputTeamB.text.clear()
        scoreA.text = "0"
        scoreB.text = "0"

        history.clearAll()
    }

    private fun appendToFocusedInput(toAppend: Char) {
        if (inputTeamA.isFocused) {
            inputTeamA.text.append(toAppend)
        } else if (inputTeamB.isFocused) {
            inputTeamB.text.append(toAppend)
        }
    }

    private fun enableSubmitButton() {
        submit.imageAlpha = 255 // 0 being transparent and 255 being opaque
        submit.isEnabled = true
    }

    private fun disableSubmitButton() {
        submit.imageAlpha = 60 // 0 being transparent and 255 being opaque
        submit.isEnabled = false
    }

    private fun chooseThemeDialog() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.choose_theme_text))
        val styles = arrayOf("Light", "Dark", "System default")

        val checkedItem =
            this.getSharedPreferences("Settings", Context.MODE_PRIVATE).getInt("Theme", 2)

        val prefs = this.getSharedPreferences("Settings", Context.MODE_PRIVATE).edit()


        builder.setSingleChoiceItems(styles, checkedItem) { dialog, which ->

            prefs.putInt("Theme", which)
            prefs.apply()

            updateTheme(which)

            dialog.dismiss()
        }

        val dialog = builder.create()
        dialog.show()
    }

    private fun updateTheme(which: Int) {
        when (which) {
            0 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            1 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            2 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        }
        delegate.applyDayNight()
    }

    private fun keepScreenOn(keepOn: Boolean) {
        if (keepOn) {
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }

        val prefs = this.getSharedPreferences("Settings", Context.MODE_PRIVATE).edit()
        prefs.putBoolean("Screen_On", keepOn)
        prefs.apply()
    }
}